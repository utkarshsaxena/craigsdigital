// This file contains all the middlewares for the web application
var middlewares = {};

middlewares.isLoggedIn = function isLoggedIn(req, res, next) {
                           var isAuth = req.isAuthenticated();
							if(isAuth) {
								req.session.userLoggedIn = true;
								 if (req.user){
                                    req.user.uniqueUserID = req.user._id.toString();
                                } 
                                return next();
							} else {
								req.session.userLoggedIn = false;
                                res.redirect('/freelancersignup');
							}
							
						};
                        
middlewares.redirectIfLoggedIn = function isLoggedIn(req, res, next) {
                           var isAuth = req.isAuthenticated();
							if(!isAuth) {
                                return next();
							} else {
								req.session.userLoggedIn = true;
                                res.redirect('/dashboard');
							}
                        };
middlewares.getUserId = function isLoggedIn(req, res, next) {
                                var isAuth = req.isAuthenticated();
                                    if(isAuth) {
                                        req.user.uniqueUserID = req.user._id.toString();
                                    }
                                    return next();
                            };

module.exports = middlewares;