'use strict';
var bcrypt = require('bcrypt-nodejs');
// This file contains the primary routes for the platform
module.exports = function(express, app, passport, Projects, Users, Gig, cloudinary, multipart) {
	var router = express.Router();
    
    // Helper functions
    function generateHash(password){
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
    }
    // =====================================
    // SIGN-UP & CREATE PROJECT PAGE
    // =====================================	
	router.post('/createproject', function(req, res, next) {
        passport.authenticate('local-signup-project', function(err, project, info){
            if (err) { return next(err); }
            if(info) {
                req.logout(); 
                res.send({info: info}) 
            };
        })(req, res, next);
    });
    
    // =====================================
    // SEARCH PROJECT PAGE
    // =====================================
	router.get('/getprojects', function(req, res){
        var query = Projects.where().sort({ 'createdAt' : -1 }).select({ "local.password" : 0});
        query.find(function(err, result) {
            if(err) {
                console.log("An error occurred while searching for recent posts: " + err.stack);
            }
            res.send(result);
        });
    });
    
    router.get('/getfilteredprojects', function(req, res){
        var categoryid = Number(req.query.categoryid);
        if (categoryid !== 0) {
            Projects.find({"local.category.id": categoryid}).sort({ 'createdAt' : -1 }).select({ "local.password" : 0}).exec(function(err, result) {			
                res.send(result);
            });            
        } else {
            var query = Projects.where().sort({ 'createdAt' : -1 }).select({ "local.password" : 0});
            query.find(function(err, result) {
                if(err) {
                    console.log("An error occurred while searching for recent posts: " + err.stack);
                }
                res.send(result);
            });
        }
    });
    
    // =====================================
    // VERIFY UNIQUE EMAIL AND PASSWORD
    // =====================================\
    router.post('/verifykeys', function(req, res, next) {
        var uniqueKey = req.body.password;
       
       Projects.find({"local.email": req.body.email}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if(project[0]){
                var i,
                    resultUniqueKey,
                    isKeyUnique,
                    l = project.length;
                for(i = 0; i<l; i++) {
                    resultUniqueKey = project[i].local.password;
                    isKeyUnique = !(bcrypt.compareSync(uniqueKey, resultUniqueKey));
                    if (!isKeyUnique) {
                        break;                    
                    }
                }
                res.send(isKeyUnique);
                    
                } else {
                    res.send(true);
                }            
        });
    });
    
    // =====================================
    // AUTHENTICATE EMAIL AND PASSWORD
    // =====================================\
    router.post('/editprojectverifyidentity', function(req, res, next) {
        var uniqueKey = req.body.auth.passwordverify;
        
        Projects.find({"local.email": req.body.auth.emailverify}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if(project[0]){
                var i,
                    resultUniqueKey,
                    keyMatch,
                    projectid,
                    l = project.length;
                for(i = 0; i<l; i++) {
                    resultUniqueKey = project[i].local.password;
                    keyMatch = bcrypt.compareSync(uniqueKey, resultUniqueKey);
                    if (keyMatch) {
                        projectid = project[i]._id;
                        break;                                            
                    }
                }
                
                if(projectid) {
                    Projects.findByIdAndUpdate(
                        {
                            _id : projectid
                        },
                        { 
                            $set:{
                                 local : {
                                        description : req.body.data.description,
                                        skillset : req.body.data.skillset,
                                        phoneNumber : req.body.data.phoneNumber,
                                        duration : req.body.data.duration,
                                        expRequired : req.body.data.expRequired,
                                        payRate : req.body.data.payRate,
                                        title : req.body.data.title,
                                        category : req.body.data.category,
                                        password: generateHash(req.body.data.password || req.body.auth.passwordverify),
                                        email : req.body.data.email || req.body.auth.emailverify,
                                        createdAt: Date.now()
                                }
                            }
                        },
                        {
                            new : true
                        },
                        function(err, data) {
                            if (err) {
                                res.send({success: false, msg: err});
                            }
                            
                            if(data) {
                                res.send({success: true, data: data});
                            }
                        });
                                     
                } else {
                    res.send({success: false, msg: "Incorrect unique key."});                    
                }                   
            } else {
                res.send({success: false, msg: "Incorrect email."});
            }            
        });
    });
	
    // =====================================
    // DELETE PROJECT
    // =====================================\
    router.post('/deleteproject/:projectid', function(req, res, next) {
        Projects.findById(req.params.projectid).select({ "local.password" : 1, "local.email" : 1}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while deleting this project: " + err.message);
			}
            if ((project.local.email === req.body.email) && (bcrypt.compareSync(req.body.uniqueKey, project.local.password))) {
                // Find the project and delete it.
                Projects.findById(req.params.projectid).remove(function(err){
                    if (err) {
                         res.send({msg:err, status: false});
                    }
                     res.send({msg:'Credentails match. Project deleted.', status: true});
                });               
            } else {
                res.send({msg:'Credentails do not match.', status: false});
            }
        });
    });
    
    // =====================================
    // SAVE AND UPDATE FREELANCER PROFILE
    // =====================================\
    router.post('/submitFreelancer', function(req, res, next) {
        Users.findByIdAndUpdate(
                        {
                            _id : req.body.userID
                        },
                        { 
                            $set:{
                                    "auth.local.email" : req.body.email,
                                    "auth.local.name" : req.body.name,
                                    "auth.local.overview" :  req.body.overview,
                                    "auth.local.skillset" : req.body.skillset,
                                    "auth.local.mainServices": req.body.mainServices,
                                    "auth.local.phoneNumber" : req.body.phoneNumber,
                                    "auth.local.availablityToWork": req.body.availablityToWork,
                                    "auth.local.experienceLevel": req.body.experienceLevel,
                                    "auth.local.profileCompleted": true,
                                    "auth.local.education":  req.body.education,
                                    "auth.local.employmentHistory": req.body.employmentHistory,
                                    "auth.local.portfolioLinks" : req.body.portfolioLinks,
                                    "auth.local.professionalTitle" : req.body.professionalTitle,
                                    "auth.local.pic.highres" : req.body.pic.highres,
                                    "auth.local.launchpadCertified": req.body.launchpadCertified,
                                    "auth.local.updatedAt": Date.now()
                            }
                        },
                        {
                            new : true
                        },
                        function(err, data) {
                            if (err) {
                                res.send({success: false, msg: err});
                            }
                            
                            if(data) {
                                res.send({success: true, data: data});
                            }
                        });
    });
    
    // =====================================
    // DELETE FREELANCER PROFILE
    // =====================================\
    router.get('/deleteuser/:userid', function(req, res, next) {
        Users.find({ _id: req.params.userid }).remove( function(err, data){
            if(err) {
                console.log(err);
            }            
            if (data) {
                res.send(true);
            }            
        });
    });
    
    // =====================================
    // UPLOAD PROFILE IMAGE
    // =====================================\
    router.post('/uploaduserprofileimage/:userid', function(req, res, next) {
        var form = new multipart.Form();
        var pictureUpdate = false,
            jsonResponse = {};
        
        function resizeImage(url){            
            var urlArray = url.split('upload');
            var newURL = urlArray[0] + 'upload/w_400' + urlArray[1];
            return newURL;
        };
        
        form.on('error', function(err) {
			console.log('Error occurred while updating user profile: ' + err.stack);
		});
        
        form.on('part', function(part) {
            if(!part.filename) {
                part.resume();
            }
            if(part.filename) {
                pictureUpdate = true;
                var stream = cloudinary.uploader.upload_stream(function(result) {
                    jsonResponse["profilePicUrl"] = resizeImage(result.url);
                    res.send(jsonResponse);
                }, {
                	folder: "profile_images"
                });
                part.pipe(stream);
                part.resume();
            }
            
            part.on('error', function(err) {
				console.log("Error occurred while stream a post image part: " + err.stack);
			});
        });
        
        // Parse req 
        form.parse(req);
    });
    
    // =====================================
    // SEARCH FREELANCERS PAGE
    // =====================================
    
    router.get('/getfreelancers', function(req, res){
        var query = Users.where().sort({ 'createdAt' : -1 }).select({ "auth.google" : 0,  "auth.facebook" : 0,  "auth.linkedin" : 0});
        query.find({ "auth.local.profileCompleted": true }, function(err, result) {
            if(err) {
                console.log("An error occurred while searching for recent posts: " + err.stack);
            }
            res.send(result);
        });
    });
    
    router.get('/getfilteredfreelancers', function(req, res){
        
        // Helper functions -- 
        function getCategoryObj(catid) {
            var category = {},
                catArray = ['View All Freelancers', 'Web & Mobile Development',  'Tutoring', 'Design & Creative', 'Blogging & Writing', 'Photography & Videography', 'Other Creative'];
            category.id = catid;
            category.name = catArray[catid];
            return category;            
        };
        
        // Query database
        var category= getCategoryObj(req.query.category);

        if ((category && category.id != 0)) {
            Users.find({'auth.local.mainServices' : category.name, "auth.local.profileCompleted": true }).sort({ 'createdAt' : -1 }).select({ "local.password" : 0}).exec(function(err, result) {			
                res.send(result);
            });
        } else {
            Users.find({"auth.local.profileCompleted": true }).sort({ 'createdAt' : -1 }).select({ "local.password" : 0}).exec(function(err, result) {			
                if(err) {
                    console.log("An error occurred while searching for recent posts: " + err.stack);
                }
                res.send(result);
            });
        }
    });
	
    // =====================================
    // GIGS API Routes
    // =====================================
	
	router.post('/gig/:userID', (req, res) => {
        const userid = req.params.userID;
        const form = new multipart.Form();
        const gig = {};
		let resJSON = {};
        Gig.collection.count({ userID: userid}, function(err, data){
            if (data < 3) {
                const resizeImage = (url) => url.replace('upload/', 'upload/w_400/');
        
                form.on('error', (err) => {
                    console.log('Error occurred while updating gigs: ' + err.stack);
                    resJSON = Object.assign(resJSON, err);
                });
                
                form.on('part', (part) => {
                    if (!part.filename) {
                        part.resume();
                    }
                    const stream = cloudinary.uploader.upload_stream((result) => {
                        if (result && result.url) {
                            gig.imageUrl = resizeImage(result.url);
                            const newGig = new Gig(gig);
                            newGig.save((err, doc) => {
                                if (err || !doc) {
                                    resJSON = Object.assign(resJSON, err);
                                }
                                resJSON = Object.assign(resJSON, doc._doc);
                                res.json(resJSON);
                            });
                        }
                        else {
                            if (!resJSON.err) {
                                resJSON = Object.assign(resJSON, {err: "An error occurred while processing an image upload to Cloudinary."});
                                res.json(resJSON);
                            }
                        }
                    }, {
                        folder: "gig_images"
                    });
                    part.pipe(stream);
                    part.resume();
                    
                    part.on('error', (err) => {
                        resJSON = Object.assign(resJSON, {err: "An error occurred while processing an image upload to Cloudinary."});
                        console.log("Error occurred while stream a post image part: " + err.stack);
                    });
                });
                
                form.on('field', (name, value) => {
                    switch (name) {
                        case 'category':
                            gig.category = value.split(",");
                            break;
                        case 'title':
                            gig.title = value;
                            break;
                        case 'description':
                            gig.description = value;
                            break;
                        case 'cost':
                            gig.cost = Number(value);
                            break;
                        case 'payRate':
                            gig.payRate = value;
                            break;
                        case 'userID':
                            gig.userID = value;
                            break;
                    }
                });
                
                form.on('close', () => {
                    if (resJSON.err) {
                        res.json(resJSON);
                    }
                });
                
                // Parse req 
                form.parse(req);
                
            } else {
                res.json(false);
            }
        });
	});
	
	router.get('/gig/:userid', (req, res) => {
		const queryParams = { userID: req.params.userid};
		const query = Gig.where(queryParams).sort({createdAt: -1}).limit(20).skip(req.body.offset || 0);
        query.exec((err, docs) => {
            if(err || !docs) {
                res.json(err);
            }
            res.json(docs);
        })
	});
    
    router.get('/allgigs', (req, res) => {
		const query = Gig.where().sort({createdAt: -1});
        query.exec((err, docs) => {
            if(err || !docs) {
                res.json(err);
            }
            res.json(docs);
        })
	});
    
    router.post('/deletegig/:gigid', (req, res) => {
        Gig.findById(req.params.gigid).remove(function(err){
            if (err) {
                res.send({msg:err, status: false});
            }
            res.send({msg:'Gig deleted.', status: true});
        });		
	});
    
    router.get('/getfilteredgigs', function(req, res){
        // Get category
        function getCategoryName(id) {
            var categoryArr = ['Web & Mobile Development', 'Tutoring', 'Design & Creative', 'Blogging & Writing', 'Photography & Videography', 'Other Creatives'];
            return categoryArr[id-1];            
        }        
        // Query database
        var category= getCategoryName(req.query.categoryid);
        if (category) {
            Gig.find({ category: { $in: [category]}}).sort({ 'createdAt' : -1 }).exec(function(err, result) {			
                res.send(result);
            });
        } else {
            Gig.where().sort({ 'createdAt' : -1 }).select({ "local.password" : 0}).exec(function(err, result) {			
                if(err) {
                    console.log("An error occurred while searching for recent posts: " + err.stack);
                }
                res.send(result);
            });
        }
    });
	
	// mount the router on the app
	// All routes relative to '/'
	app.use('/api', router);
}