'use strict';
// This file contains the primary routes for the platform
module.exports = function(express, app, passport, Projects, Users, env) {
	var router = express.Router();

	// Route middlewares to check if a user is logged in
	var middlewares = require('./middlewares.js');

	// =====================================
    // LOG-IN PAGE
    // =====================================
	router.get('/', middlewares.getUserId, function(req, res){
		res.render('index', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});

    // =====================================
    // POST PROJECT PAGE
    // =====================================
	router.get('/searchprojects', middlewares.getUserId, function(req, res){
		res.render('searchProjects', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false
		});
	});

    // =====================================
    // SEARCH PROJECT PAGE
    // =====================================
	router.get('/postproject', middlewares.redirectIfLoggedIn, function(req, res){
		res.render('postproject', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});

    // =====================================
    // ABOUT-US PAGE
    // =====================================
	router.get('/aboutus', middlewares.getUserId, function(req, res){
		res.render('aboutUs', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false
		});
	});

    // =====================================
    // INDIVIDUAL PROJECT PAGE
    // =====================================
	router.get('/projects/:projectid', middlewares.getUserId, function(req, res){
		Projects.findById(req.params.projectid).select({ "local.password" : 0}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if (project) {
                project.projectID = String(project._id);
            }
            
             res.render('project', {
                project : project,
                userid: (req.user) ? req.user.uniqueUserID : false,
				env: env
            });
        });
	});

    // =====================================
    // EDIT INDIVIDUAL PROJECT PAGE
    // =====================================
	router.get('/editproject/:projectid', function(req, res){
		Projects.findById(req.params.projectid).select({ "local.password" : 0}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if (project) {
                project.projectID = String(project._id);
            }
             res.render('editProject', {
                project : project,
                userid: (req.user) ? req.user.uniqueUserID : false,
				env: env
            });
        });
	});

    router.get('/editprojectdata/:projectid', function(req, res){
		Projects.findById(req.params.projectid).select({ "local.password" : 0}).exec(function(err, project){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if (project) {
                project.projectID = String(project._id);
            }
             res.send(project);
        });
	});

    // =====================================
    // FREELANCERS PAGE
    // =====================================
	router.get('/freelancersignup', middlewares.redirectIfLoggedIn, function(req, res){
		res.render('freelancersignup', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
            errorMessage: req.flash('signupMessage'),
			env: env
		});
	});
    
    // ---- Local Auth ----	    
    router.post('/auth/createuser', passport.authenticate('local-signup', {
        successRedirect: '/dashboard',
        failureRedirect: '/freelancersignup',
        failureFlash: true // Show flash messages
    }));
    
    router.post('/auth/signin', passport.authenticate('local-login', {
        successRedirect: '/dashboard',
        failureRedirect: '/signin',
        failureFlash: true // Show flash messages
    }));
    
    
    // ---- Facebook Auth ----
    router.get('/auth/facebook', passport.authenticate('facebook'));

    router.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successRedirect: '/dashboard',
        failureRedirect: '/freelancersignup'
    }));
    // ---- Facebook Auth End ----

    // ---- LinkedIn Auth -----
    router.get('/auth/linkedin', passport.authenticate('linkedin'));

    router.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
        successRedirect: '/dashboard',
        failureRedirect: '/freelancersignup'
    }));
    // ---- Linkedin auth end -----

    // ---- Google Auth ----
    router.get('/auth/google',
        passport.authenticate('google', { scope:
            [ 'https://www.googleapis.com/auth/plus.login',
            , 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
        ));

        router.get('/auth/google/callback',
            passport.authenticate( 'google', {
                successRedirect: '/dashboard',
                failureRedirect: '/freelancersignup'
        }));
    // ---- Google Auth End ----

    // =====================================
    // DASHBOARD PAGE
    // =====================================

    router.get('/dashboard', middlewares.isLoggedIn, function(req, res){
		res.render('dashboardfreelancer', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});

    router.get('/edituserdata/:userid', function(req, res){
		Users.findById(req.params.userid).select({ "auth.facebook" : 0, "auth.google" : 0, "auth.linkedin" : 0}).exec(function(err, user){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if (user) {
                user.projectID = String(user._id);
            }
             res.send(user);
        });
	});

    // =====================================
    // LOGOUT
    // =====================================
    router.get('/logout', function(req, res, next) {
        req.session.userLoggedIn = false;
        req.logout();
        res.redirect('/');
    });

    // =====================================
    // FREELANCERS PAGE
    // =====================================
	router.get('/signin', middlewares.redirectIfLoggedIn, function(req, res){
		res.render('signin', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
            errorMessage: req.flash('loginMessage'), 
			env: env
		});
	});

    // =====================================
    // FREELANCERS PAGE
    // =====================================
	router.get('/searchfreelancers',  middlewares.getUserId, function(req, res){
		res.render('searchFreelancers', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});

    // =====================================
    // INDIVIDUAL FREELANCER PAGE
    // =====================================
	router.get('/users/:userid', middlewares.getUserId, function(req, res){
		Users.findById(req.params.userid).select({ "auth.facebook" : 0, "auth.google" : 0, "auth.linkedin" : 0}).exec(function(err, user){
            if(err) {
				console.log("An error occurred while retrieving this project: " + err.message);
			}
            if (user) {
                user.userID = String(user._id);
            }
            
           if (user.auth.local.profileCompleted) {
               res.render('freelancer', {
                    user : user,
                    userid: (req.user) ? req.user.uniqueUserID : false,
                    env: env
                });               
           } else {
               res.redirect('/dashboard');
           }
             
        });
	});
    
    // =====================================
    // Gigs PAGE
    // =====================================
	router.get('/searchgigs', middlewares.getUserId, function(req, res){
		res.render('searchGigs', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});
    
    router.get('/postgigs', middlewares.isLoggedIn, function(req, res){
		res.render('postGigsPage', {
			isUserLoggedIn: req.session.userLoggedIn || false,
            userid: (req.user) ? req.user.uniqueUserID : false,
			env: env
		});
	});

	// mount the router on the app
	// All routes relative to '/'
	app.use('/', router);
}
