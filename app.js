/* global __dirname */
/* global process */
// Get all modules
var express = require('express'),
	app = express(),
	cons = require('consolidate'),
	path =  require('path'),
	swig = require('swig'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	flash = require('connect-flash'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	ConnectMongo = require('connect-mongo')(session),
    cloudinary = require('cloudinary'),
    multipart = require('multiparty'),
	port = process.env.PORT || 5000,
	config = require('./config/config'),
    Projects = require('./models/projects'),
    Users = require('./models/users'),
    Gig = require('./models/gigs');


// Get environment variable
var env = process.env.NODE_ENV || 'development';
app.use(cookieParser()); // Read cookies for authentication
if(env === 'development') {
	console.log(config.message);
	// Express middleware to populate 'req.cookies' so we can access cookies
    // Set up our application	
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        cookie: {}
    })); // saving cookies to session locally
	
} else {
	// Set up our application	
	console.log(config.message);
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        store: new ConnectMongo({
            url: config.dbURL,
            stringify: true        
        })    
    })); // Saving cookies to mongodb when in production
}

// Resolve paths to client files.
var client_files = path.resolve(__dirname, './client/');

// Connect to MongoDB
mongoose.connect(config.dbURL, function(err, db) {
	if(err) {
        console.log('Error at mongoose.connect');
        console.error(err);
    };
	
	// Pass in passport configuration file
	// Needs to be configured before sending to routes
	require('./auth/passport')(passport, config);
	
	// Register our templating engine
	app.engine('html', cons.swig);
	app.set('view engine', 'html');
	app.set('views', __dirname + '/views');
	
	// Express middleware to serve static files
	app.use(express.static(__dirname + '/assets'));
	
	// Express middleware to populate 'req.body' so we can access POST variables
	app.use(bodyParser.json()); // Parsing html forms
	app.use(bodyParser.urlencoded({ extended: true }));
	
	// Set up passport
	app.use(passport.initialize());
	app.use(passport.session()); // Persisting log-in sessions
	app.use(flash()); // For flash messages stored in session
    
    // Set up cloudinary
    cloudinary.config(config.cloudinary);
    
	// Get routes from routes/main.js
	require('./routes/main.js')(express,app, passport, Projects, Users, env);
    require('./routes/api.js')(express, app, passport, Projects, Users, Gig, cloudinary, multipart);
	
	app.listen(port, function() {
		console.log('Node app is running on port',
		port);
	});	
});