/* global $ */
// Tag it code for become a freelancer form

$(document).ready(function() {
        $("#skills-tags").tagit({
            tagLimit: 10,
            placeholderText: "Enter skills needed",
            removeConfirmation: true,
            allowSpaces: true,
            allowDuplicates: false,
            preprocessTag: function(val) {
                if (!val) { return ''; }
                return val[0].toUpperCase() + val.slice(1, val.length);
            }
        });
        
        $("#skills-tags-two").tagit({
            tagLimit: 10,
            placeholderText: "Enter your skills",
            removeConfirmation: true,
            allowSpaces: true,
            allowDuplicates: false, 
            preprocessTag: function(val) {
                if (!val) { return ''; }
                return val[0].toUpperCase() + val.slice(1, val.length);
            }
        });
        
        var servicesData = ['Web & Mobile Development', 'Tutoring', 'Design & Creative', 'Blogging & Writing', 'Photography & Videography', 'Other Creatives'];
        $("#services-tags").tagit({
            tagLimit: 5,
            placeholderText: "Select services",
            removeConfirmation: true,
            interactive: false,
            showAutocompleteOnFocus:true,
            beforeTagAdded: function(event, ui) {
            // Only add tag if it exists in the array
            if($.inArray(ui.tagLabel, servicesData)==-1) return false;        
        },
            autocomplete: {
                delay: 0,
                minLength: 0,
                source: servicesData,
                appendTo: "#autocomplete-container-two"                
            }
        });
});