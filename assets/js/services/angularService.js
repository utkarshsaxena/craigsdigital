"use strict";
// Angular services for the entire website
craigsdigitalApp.service('craigsdigitalService', function($http) {
    
    this.submitProject = function($scope, formData) {
        var req = {
            method: 'POST',
            url: '/api/verifykeys',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                email: $scope.email,
                password: $scope.password
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            if (data) {
                $scope.keyisnotunique = false;
                // Email and password is unique, proceed to save the form
                var req = {
                    method: 'POST',
                    url: '/api/createproject',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: formData
                };
                
                $http(req).success(function(data, status, headers, config){
                    $scope.success = data.info;
                    $scope.disableSubmit = true;
                    $scope.submitSuccess = true;
                    $scope.showLoading = false;
                }).error(function(data, status, headers, config){
                    $scope.submitSuccess = false;
                    $scope.submitError = true;
                    $scope.disableSubmit = false;
                });
                // END               
            } else {
                $scope.keyisnotunique = true;
                $scope.disableSubmit = false;
                $scope.showLoading = false;
            }
            
        }).error(function(data, status, headers, config){
            $scope.submitSuccess = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
        })
    };
    
    this.getAllProjects = function($scope, URL) {
        var req = {
            method: 'GET',
            url: URL,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.projectsData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
    
    this.getFilteredProjects = function($scope, data) {
        var req = {
            method: 'GET',
            url: '/api/getfilteredprojects?categoryid=' + $scope.selectedCategory.id,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.projectsData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
    
    this.getProjectData = function($scope) {
        var req = {
            method: 'GET',
            url: '/editprojectdata/' + $scope.projectid,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
           $scope.description = data.local.description;
           $scope.title = data.local.title;
           $scope.phoneNumber = data.local.phoneNumber || '';
           $scope.skillsRequired = data.local.skillset;
           $scope.payRateSelected = data.local.payRate;
           $scope.categorySelected = data.local.category;
           $scope.durationSelected = data.local.duration;
           $scope.experienceLevelSelected = data.local.expRequired;
           
        }).error(function(data, status, headers, config){
            console.error(data);
        });        
    };
    
    this.updateProject = function($scope, formData, verifyData) {
        var req = {
            method: 'POST',
            url: '/api/editprojectverifyidentity',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                auth: verifyData,
                data: formData                
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            if (data && data.success) {
                $scope.disableSubmit = true;
                $scope.submitSuccess = true;
                $scope.showLoading = false;              
            } else {
                $scope.incorrectCreds = true;
                $scope.disableSubmit = false;
                $scope.showLoading = false;
            }
            
        }).error(function(data, status, headers, config){
            $scope.submitSuccess = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
        })
    };
    
     this.submitFreelancer = function($scope, formData) {
         
         // Handle submitting data to the server
         function submitFormData(formData){
              var request = {
                        method: 'POST',
                        url: '/api/submitFreelancer',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: formData
                    };
                    
                    $http(request).success(function(dataObj, status, headers, config){
                        $scope.success = dataObj.success;
                        $scope.email = dataObj.data.auth.local.email;
                        $scope.name = dataObj.data.auth.local.name;
                        $scope.phoneNumber = dataObj.data.auth.local ? dataObj.data.auth.local.phoneNumber : '';
                        $scope.imageURL = dataObj.data.auth.local.pic ? dataObj.data.auth.local.pic.highres : '';
                        $scope.title = dataObj.data.auth.local.professionalTitle;
                        $scope.overview = dataObj.data.auth.local.overview;
                        $scope.existingSkills = dataObj.data.auth.local.skillset;
                        $scope.existingServices = dataObj.data.auth.local.mainServices;
                        $scope.availabilitySelected = dataObj.data.auth.local.availablityToWork.id;
                        $scope.experienceLevelSelected = dataObj.data.auth.local.experienceLevel.id; 
                        if (dataObj.data.auth.local.education) {
                            $scope.schoolDesc = dataObj.data.auth.local.education.school;
                            $scope.schoolDescOne = dataObj.data.auth.local.education.schoolOne;                
                        }     
                        
                        if (dataObj.data.auth.local.employmentHistory[0]) {
                                $scope.companyName = dataObj.data.auth.local.employmentHistory[0].company;
                                $scope.titleName = dataObj.data.auth.local.employmentHistory[0].title;
                                $scope.empHist = dataObj.data.auth.local.employmentHistory[0].desc;              
                        }  
                                    
                        if (dataObj.data.auth.local.portfolioLinks) {
                            $scope.linkOne = dataObj.data.auth.local.portfolioLinks.portfolioUrl;
                            $scope.linkTwo = dataObj.data.auth.local.portfolioLinks.linkedinUrl;
                            $scope.linkThree = dataObj.data.auth.local.portfolioLinks.codeRepoUrl;
                            $scope.linkFour = dataObj.data.auth.local.portfolioLinks.otherUrl;              
                        }
                        
                        $scope.disableSubmit = false;
                        $scope.submitSuccess = true;
                        $scope.showLoading = false;
                    }).error(function(data, status, headers, config){
                        $scope.submitSuccess = false;
                        $scope.submitError = true;
                        $scope.disableSubmit = false;
                    });
         };
         
         var jQuery = $,
            $uploadedFile = jQuery('#uploadImage');
         
         if($uploadedFile.val() != ''){
            var form = new FormData();
            form.append("imageFile", $uploadedFile[0].files[0]);
            jQuery.ajax({
                url: '/api/uploaduserprofileimage/' + $scope.userID,
                data: form,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data, status, req) {
                    formData.pic.highres = data.profilePicUrl;
                    submitFormData(formData);                    
                },
                error:function (req, status, error) {
                    console.error(error);
                }
            });             
         } else {
             submitFormData(formData);
         }
         
    };
    
    this.getUserData = function($scope) {
        var req = {
            method: 'GET',
            url: '/edituserdata/' + $scope.userID,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
           $scope.profileCompleted = data.auth.local.profileCompleted;
           $scope.email = data.auth.local.email;
           $scope.name = data.auth.local.name;
           $scope.phoneNumber = data.auth.local ? data.auth.local.phoneNumber : '';
           $scope.imageURL = data.auth.local.pic ? data.auth.local.pic.highres : '';
           $scope.title = data.auth.local.professionalTitle;
           $scope.overview = data.auth.local.overview;
           $scope.existingSkills = data.auth.local.skillset;
           $scope.existingServices = data.auth.local.mainServices;
           $scope.availabilitySelected = data.auth.local.availablityToWork;
           $scope.experienceLevelSelected = data.auth.local.experienceLevel; 
           if (data.auth.local.education) {
               $scope.schoolDesc = data.auth.local.education.school;
               $scope.schoolDescOne = data.auth.local.education.schoolOne;                
           }     
           
           if (data.auth.local.employmentHistory[0]) {
                $scope.companyName = data.auth.local.employmentHistory[0].company;
                $scope.titleName = data.auth.local.employmentHistory[0].title;
                $scope.empHist = data.auth.local.employmentHistory[0].desc;               
           }  
                     
          if (data.auth.local.portfolioLinks) {
              $scope.linkOne = data.auth.local.portfolioLinks.portfolioUrl;
              $scope.linkTwo = data.auth.local.portfolioLinks.linkedinUrl;
              $scope.linkThree = data.auth.local.portfolioLinks.codeRepoUrl;
              $scope.linkFour = data.auth.local.portfolioLinks.otherUrl;              
          }
          $scope.disableSubmit = false;
        }).error(function(data, status, headers, config){
            console.error(data);
        });        
    };
    
    this.deleteUser = function($scope) {
        var req = {
            method: 'GET',
            url: '/api/deleteuser/' + $scope.userID,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
           if(data === true) {
               window.location.replace("/logout");
           }         
        }).error(function(data, status, headers, config){
            console.error(data);
        });        
    };
    
    this.getAllFreelancers = function($scope, URL) {
        var req = {
            method: 'GET',
            url: URL,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.freelancersData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
    
    this.getFilteredFreelancers = function($scope, data) {
        var req = {
            method: 'GET',
            url: '/api/getfilteredFreelancers?category=' + ($scope.selectedCategory ? $scope.selectedCategory.id  : null),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.freelancersData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
});