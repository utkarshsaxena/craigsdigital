// Service for create a gig page.

craigsdigitalApp.service('gigsService', function($http) {
    
    this.getAllGigs = function($scope, URL) {
        var req = {
            method: 'GET',
            url: URL,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.allGigsData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
    
    this.getFilteredGigs = function($scope, data) {
        var req = {
            method: 'GET',
            url: '/api/getfilteredGigs?categoryid=' + ($scope.selectedCategory ? $scope.selectedCategory.id  : null),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        
        $http(req).success(function(data, status, headers, config){
            $scope.allGigsData = data;
            $scope.hideLoader = true;
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    };
    
    this.deleteGig = function($scope) {
        
    };
    
    this.submitGig = function($scope, category) {
        var jQuery = $,
            $uploadedFile = jQuery('#uploadImage');
        
             if($uploadedFile.val() != '') {
                  var imageFile = $uploadedFile[0].files[0];
             } else {
                 imageFile = null;
             }
       
            var form = new FormData();
            
            form.append("imageFile", imageFile);
            form.append("description", $scope.description);
            form.append("cost", $scope.cost);
            form.append("payRate", $scope.payRateSelected.name);
            form.append("userID", String($scope.userID));
            form.append("title", $scope.title);
            form.append("category", category);
            
            jQuery.ajax({
                url: '/api/gig/' + $scope.userID,
                data: form,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data, status, req) {
                    if (!data) {
                        $scope.submitSuccess = false;
                        $scope.submitError = true;
                        $scope.disableSubmit = false; 
                        $scope.showLoading = false;                                                
                    } else {
                        $scope.disableSubmit = false;
                        $scope.submitSuccess = true;
                        $scope.showLoading = false;
                        setTimeout(function(){ location.reload(); }, 2000);                        
                    }                    
                    $scope.$apply();                   
                },
                error:function (req, status, error) {
                    $scope.submitSuccess = false;
                    $scope.submitError = true;
                    $scope.disableSubmit = false;
                }
            });       
    };
    
});