"use strict";
/* global $ */
// Main js file.

// Toggle how it works section

$('.how-it-works-btn').on('click', function(event){
    event.preventDefault();
    $('.mdl-grid:nth-child(1)').css('display', 'flex');
    $('.jumbotron-button-two').show();
    $('.how-it-works-btn').hide();
});

$('.hide-value-prop-section').on('click', function(event){
    event.preventDefault();
    $('.how-it-works-container').fadeOut(200);
    $('.jumbotron-button-two').hide();
    $('.how-it-works-btn').show();
});

// Handle landing page clicks
$('a.search-project-link').on('click', function(event){
    event.preventDefault();
    
    if(typeof(sessionStorage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        sessionStorage.setItem('categoryEndPoint', $(event.target).attr("data-ref"));
    }
    window.location.href = $(event.target).attr("data-href");
});

$('a.search-freelancer-btn').on('click', function(event){
    event.preventDefault();
    if(typeof(sessionStorage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        sessionStorage.setItem('freelancerEndPoint', $(event.target).closest("a.search-freelancer-btn").attr("data-ref"));
    }
    window.location.href = "/searchfreelancers";
});

$('.js-mdl-cards').on('click', function(event){
    event.preventDefault();
    if($(event.target).attr('class') !== 'search-project-link') {
        if(typeof(sessionStorage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            sessionStorage.setItem('categoryEndPoint', $(event.target).attr("data-ref"));
        }
        window.location.href = $(event.target).attr("data-href");        
    }    
});

// Delete Project Button handler
$('#del-btn').on('click', function(){
    $('.delete-btn').click();
});
$('.delete-btn').on('click', function(event){
    event.preventDefault();
    
    var confirmMessage = confirm("Are you sure you want to delete this project? This action cannot be undone.");
    if (confirmMessage === true) {
        $('.del-proj-spinner').css("display", "inline-block");
        $('.delete-btn').css("display", "none");
        // Pressed Okay
        var email = prompt("Please enter the email associated with this project.");
        var uniqueKey = prompt("Please enter the unique key associated with this project.");
        if (email && uniqueKey) {
            $.ajax({
            method: "POST",
            url: "/api/deleteproject/" + event.currentTarget.attributes[1].value,
            data: { email: email, uniqueKey: uniqueKey }
            }).done(function( data ) {
                alert( data.msg );
                
                if(data.status) {
                    window.location.replace("/");
                } else {
                    $('.delete-btn').css("display", "inline-block");
                    $('.del-proj-spinner').css("display", "none");
                }
            }).fail(function(error) {
                alert( "Error: " +  error);
                $('.delete-btn').css("display", "inline-block");
                $('.del-proj-spinner').css("display", "none");
            });
        } else {
            $('.delete-btn').css("display", "inline-block");
            $('.del-proj-spinner').css("display", "none");
            
        }
    } else {
       // Pressed cancel - do nothing
    }
});