/* global jQuery */
// Post gigs page
$( document ).ready(function() {
        jQuery.ajax({
        url: '/api/gig/' + $('.manage-gigs-container').attr('data-userid'),
        type: 'GET',
        success: function (data, status, req) {
            if(data[0]) {                    
                for(var i=0, l=data.length; i < l; i++) {
                    
                    var html = [
                        '<div class="mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--12-col-phone">',
                        '<div class="demo-card-square mdl-card mdl-shadow--2dp">',
                        '<div class="mdl-card__title mdl-card--expand" style="background: url(' + data[i].imageUrl + ') center center no-repeat #46B6AC">',
                        '<h2 class="mdl-card__title-text">' + data[i].title + '</h2>',
                        '</div>',
                        '<div class="mdl-card__actions mdl-card--border">',
                        '<a data-id="' + data[i]._id + '" class="delete-gig-btn mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">',
                        'Delete Gig',
                        '</a></div></div'
                        ];
                    $('.gigs-container .mdl-grid').append(html.join(''));
                }                    
            } else {
                $('.gigs-container .mdl-grid').append('<h5 style="color: #F44336;">No gigs found. Scroll below to post a gig.</h5>');                
            }
            
            $('.delete-gig-btn').on('click', function(event){
                event.preventDefault();
                var confirmMessage = confirm('Are you sure you want to delete this gig? This action cannot be undone.');
                if(confirmMessage === true) {
                    jQuery.ajax({
                        url: '/api/deletegig/' + $(event.target).closest('a').attr('data-id'),
                        type: 'POST',
                        success: function (data, status, req) {
                            alert('You gig has been successfully removed.');
                            location.reload();
                        }, error:function (req, status, error) {
                            console.log(error);
                            
                        }
                    });
                } else {
                    return false;
                }                        
            });                
        },
        error:function (req, status, error) {
            console.log(error);
        }
    });            
});