/* global craigsdigitalApp */
"use strict";
/* global searchProjectController */
// Angular directive for searching freelancers functionality.
// <div search-project-directive></div>
craigsdigitalApp.directive('searchGigsDirective', function () {
    return {
        restrict: 'EA',        
        scope: {
            gigs: '=',
            filter: '=',
            loader: '='
        },
        templateUrl: '/angularTemplates/searchGigsDirective.html',
        link: function ($scope, element, attrs) {
                        
            
         }
    }
});