"use strict";
/* global searchProjectController */
// Angular directive for searching freelancers functionality.
// <div search-project-directive></div>
craigsdigitalApp.directive('searchProjectDirective', function () {
    return {
        restrict: 'EA',        
        scope: {
            projects: '=',
            filter: '=',
            loader: '='
        },
        templateUrl: '/angularTemplates/searchProjectsDirective.html',
        link: function ($scope, element, attrs) {
                        
            
         }
    }
});