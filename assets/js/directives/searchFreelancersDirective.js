/* global craigsdigitalApp */
"use strict";
/* global searchProjectController */
// Angular directive for searching freelancers functionality.
// <div search-project-directive></div>
craigsdigitalApp.directive('searchFreelancersDirective', function () {
    return {
        restrict: 'EA',        
        scope: {
            freelancers: '=',
            filter: '=',
            loader: '='
        },
        templateUrl: '/angularTemplates/searchFreelancersDirective.html',
        link: function ($scope, element, attrs) {
                        
            
         }
    }
});