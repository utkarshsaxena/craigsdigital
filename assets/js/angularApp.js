"use strict";
/* global angular */
// Angular app. for the entire website.
var craigsdigitalApp = angular.module('craigsdigitalApp', []);

craigsdigitalApp.config (['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
}]);