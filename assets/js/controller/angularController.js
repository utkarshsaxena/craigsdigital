/* global $ */
"use strict";
/* global craigsdigitalApp */
// Angular controller for the entire website

// Helper functions
// CSV to array
function csvToArray(csv) {
    if(csv) {
        return csv.split(',').sort();            
    } else {
        return [];
    }        
};

function getSkillsFromDOM(){
        var skillArray = $.map( $('#skills-tags li .tagit-label'), function( val, i ) {            
            return $(val).text();
        });
        return skillArray;
    };
    
craigsdigitalApp.controller('postProjectFormController',  ['$scope', 'craigsdigitalService', function($scope, craigsdigitalService) {
    // Helper functions
    // Initialize select options
    $scope.categoryList = [
        { id: 1, name: 'Web & Mobile Development'},
        { id: 2, name: 'Tutoring '},
        { id: 3, name: 'Design & Creative'},
        { id: 4, name: 'Blogging & Writing'},
        { id: 5, name: 'Photography & Videography'},
        { id: 6, name: 'Other Creative Projects'}
    ];
    
    $scope.payRateOptions = [
        { id: 1, name: 'Pay by the hour', desc: "Hourly contracts are appropriate when you're paying for the freelancer's time rather than a finite deliverable." },
        { id: 2, name: 'Pay a fixed price', desc: "Fixed-price contracts work well for projects with finite deliverables and a clear end. You and your freelancer agree to certain project milestones. Payments are made when the milestones have been completed to your satisfaction." },
        { id: 3, name: 'To be decided', desc: "Not a recommended option. More the freelancer knows about the project the better."}
    ];
    
    $scope.experienceLevelOptions = [
        { id: 1, name: 'Entry Level', desc: 'I am looking for a freelancer with the  lowest rates and limited skills.' },
        { id: 2, name: 'Intermediate', desc: 'I am looking for a mix of experience and value.'  },
        { id: 3, name: 'Expert', desc: 'I am willing to pay higher rates for the most experienced freelancers.' }
    ];
    
    $scope.durationOptions = [
        { id: 1, name: 'More than 6 months'},
        { id: 2, name: '3 to 6 months'},
        { id: 3, name: '1 to 3 months' },
        { id: 4, name: 'Less than 1 month'},
        { id: 5, name: 'Less than 1 week'},
        { id: 6, name: 'Undecided' }
    ];
    
    $scope.timeOptions = [
        { id: 1, name: 'More than 30 HRS/WEEK'},
        { id: 2, name: 'Less than 30 HRS/WEEK'},
        { id: 3, name: '1 to 3 months' },
        { id: 6, name: "I don't know yet" }
    ];
    
    $scope.showHelpText = function(){
        $scope.helpTextVisible = true;
    }
    
    // handle submit form
    $scope.submitForm = function(isValid) {
        $scope.submitted = true;
        $scope.disableSubmit = true;
        // check to make sure the form is completely valid        
        if (isValid) {
            var formData = {
                category : {id: $scope.categorySelected.id, name: $scope.categorySelected.name},
                title : $scope.title,
                description : $scope.description,
                payRate : {id: $scope.payRateSelected.id, name: $scope.payRateSelected.name},
                expRequired : {id: $scope.experienceLevelSelected.id, name: $scope.experienceLevelSelected.name},
                duration : {id: $scope.durationSelected.id, name: $scope.durationSelected.name},
                email : $scope.email,
                launchpadCertified: false,
                phoneNumber: $scope.phoneNumber || null,
                password: $scope.password,
                skillset: csvToArray($scope.skillsRequired)
            };
            $scope.showLoading = true;
            craigsdigitalService.submitProject($scope, formData);
            $scope.disableSubmit = true;
        } else {
            $scope.submitSuccess = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
            $scope.showLoading = false;
        }
    };
}]);

craigsdigitalApp.controller('searchProjectController',  ['$scope', 'craigsdigitalService', function($scope, craigsdigitalService) {
    var redirectURL;
    $scope.hideLoader = false;
    // Get all projects
     if(typeof(sessionStorage) !== "undefined") {
          redirectURL = sessionStorage.getItem('categoryEndPoint') || '/api/getprojects';
          sessionStorage.removeItem('categoryEndPoint');
     } else {
         redirectURL = '/api/getprojects';
     }
    
    craigsdigitalService.getAllProjects($scope, redirectURL);
    
    // Get filtered products and update product list
    $scope.filterProjects = function(){
        $scope.hideLoader = false;
        craigsdigitalService.getFilteredProjects($scope);
    };
    
    // Initialize
    $scope.categoryList = [
        { id: 0, name: 'View All'},
        { id: 1, name: 'Web & Mobile Development'},
        { id: 2, name: 'Tutoring '},
        { id: 3, name: 'Design & Creative'},
        { id: 4, name: 'Blogging & Writing'},
        { id: 5, name: 'Photography & Videography'},
        { id: 6, name: 'Other Creative Projects'}
    ];
  
}]);


craigsdigitalApp.controller('editProjectFormController',  ['$scope', 'craigsdigitalService', function($scope, craigsdigitalService) {
    // Initialize select options
    $scope.categoryList = [
        { id: 1, name: 'Web & Mobile Development'},
        { id: 2, name: 'Tutoring '},
        { id: 3, name: 'Design & Creative'},
        { id: 4, name: 'Blogging & Writing'},
        { id: 5, name: 'Photography & Videography'},
        { id: 6, name: 'Other Creative Projects'}
    ];
    
    $scope.payRateOptions = [
        { id: 1, name: 'Pay by the hour', desc: "Hourly contracts are appropriate when you're paying for the freelancer's time rather than a finite deliverable." },
        { id: 2, name: 'Pay a fixed price', desc: "Fixed-price contracts work well for projects with finite deliverables and a clear end. You and your freelancer agree to certain project milestones. Payments are made when the milestones have been completed to your satisfaction." },
        { id: 3, name: 'To be decided', desc: "Not a recommended option. More the freelancer knows about the project the better."}
    ];
    
    $scope.experienceLevelOptions = [
        { id: 1, name: 'Entry Level', desc: 'I am looking for a freelancer with the  lowest rates and limited skills.' },
        { id: 2, name: 'Intermediate', desc: 'I am looking for a mix of experience and value.'  },
        { id: 3, name: 'Expert', desc: 'I am willing to pay higher rates for the most experienced freelancers.' }
    ];
    
    $scope.durationOptions = [
        { id: 1, name: 'More than 6 months'},
        { id: 2, name: '3 to 6 months'},
        { id: 3, name: '1 to 3 months' },
        { id: 4, name: 'Less than 1 month'},
        { id: 5, name: 'Less than 1 week'},
        { id: 6, name: 'Undecided' }
    ];
    
    $scope.timeOptions = [
        { id: 1, name: 'More than 30 HRS/WEEK'},
        { id: 2, name: 'Less than 30 HRS/WEEK'},
        { id: 3, name: '1 to 3 months' },
        { id: 6, name: "I don't know yet" }
    ];
    
    $scope.showHelpText = function(){
        $scope.helpTextVisible = true;
    }
    
    $scope.$watch('projectid',function(newValue, oldValue) {
        if(newValue) {
            craigsdigitalService.getProjectData($scope);
        }
    });
    
    // handle submit form
    $scope.updateForm = function(isValid) {
        $scope.submitted = true;
        $scope.disableSubmit = true;
        // check to make sure the form is completely valid        
        if (isValid) {
            var formData = {
                category : {id: $scope.categorySelected.id, name: $scope.categorySelected.name},
                title : $scope.title,
                description : $scope.description,
                launchpadCertified: false,
                payRate : {id: $scope.payRateSelected.id, name: $scope.payRateSelected.name},
                expRequired : {id: $scope.experienceLevelSelected.id, name: $scope.experienceLevelSelected.name},
                duration : {id: $scope.durationSelected.id, name: $scope.durationSelected.name},
                email : $scope.email || null,
                phoneNumber: $scope.phoneNumber || null,
                password: $scope.password || null,
                skillset: getSkillsFromDOM()
            };
            
            var verifyData = {
                emailverify : $scope.emailverify,
                passwordverify : $scope.passwordverify
            };
            
            $scope.showLoading = true;
            craigsdigitalService.updateProject($scope, formData, verifyData);
            $scope.disableSubmit = true;
        } else {
            $scope.submitSuccess = false;
            $scope.showLoading = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
        }
    };
}]);

craigsdigitalApp.controller('becomeFreelancerFormController',  ['$scope', 'craigsdigitalService', function($scope, craigsdigitalService) {    
    // Initialize select options    
    $scope.availabilityOptions = [
        { id: 1, name: 'Less than 30 hrs / week'},
        { id: 2, name: 'More than 30 hrs / week' },
        { id: 3, name: 'As needed - Open to offers'}
    ];
    
    $scope.experienceLevelOptions = [
        { id: 1, name: 'Entry Level', desc: 'Starting to build experience in my field.' },
        { id: 2, name: 'Intermediate', desc: 'A few years of professional experience in my field.'  },
        { id: 3, name: 'Expert', desc: 'Many years of professional experience doing complex projects.' }
    ];
    
    $scope.durationOptions = [
        { id: 1, name: 'More than 6 months'},
        { id: 2, name: '3 to 6 months'},
        { id: 3, name: '1 to 3 months' },
        { id: 4, name: 'Less than 1 month'},
        { id: 5, name: 'Less than 1 week'},
        { id: 6, name: 'Undecided' }
    ];
    
    $scope.timeOptions = [
        { id: 1, name: 'More than 30 HRS/WEEK'},
        { id: 2, name: 'Less than 30 HRS/WEEK'},
        { id: 3, name: '1 to 3 months' },
        { id: 6, name: "I don't know yet" }
    ];
    
    $scope.showHelpText = function(){
        $scope.helpTextVisible = true;
    }
    
    // Get user data and show in form
    $scope.$watch('userID',function(newValue, oldValue) {
        if(newValue) {
            craigsdigitalService.getUserData($scope);
        }
    });
    
    // Handle Delete User
    $scope.deleteUser = function(event) {
        event.preventDefault();
        var confirmMessage = confirm('Are you sure you want to delete your account? This action cannot be undone.');
        if(confirmMessage === true) {
            craigsdigitalService.deleteUser($scope);                        
        }        
    };
    // handle submit form
    $scope.submitForm = function(isValid) {
        var URLify  = function(url) {
            if (url.indexOf('http') === -1 ) {
                return 'http://' + url;
            } else {
                return url;
            }            
        }
        			
        $scope.submitted = true;
        $scope.disableSubmit = true;
        // check to make sure the form is completely valid        
        if (isValid) {
            var formData = {
                email : $scope.email,
                name: $scope.name,
                phoneNumber: $scope.phoneNumber || null,
                professionalTitle : $scope.title,
                overview : $scope.overview,
                skillset: $scope.skillsRequired ? csvToArray($scope.skillsRequired) : $scope.existingSkills,
                mainServices: $scope.servicesRequired ? csvToArray($scope.servicesRequired) : $scope.existingServices,
                availablityToWork : {id: $scope.availabilitySelected.id, name: $scope.availabilitySelected.name},
                experienceLevel : {id: $scope.experienceLevelSelected.id, name: $scope.experienceLevelSelected.name},
                education : {
                    school : $scope.schoolDesc,
                    schoolOne: $scope.schoolDescOne ? $scope.schoolDescOne : ''                                        
                },
                employmentHistory: [
                    {
                        company: $scope.companyName,
                        title : $scope.titleName,
                        desc: $scope.empHist
                    }
                ],
                portfolioLinks: {
                    portfolioUrl: URLify($scope.linkOne),
                    linkedinUrl: $scope.linkTwo ? URLify($scope.linkTwo) : '',
                    codeRepoUrl: $scope.linkThree ? URLify($scope.linkThree) : '',
                    otherUrl: $scope.linkFour ? URLify($scope.linkFour) : ''                                        
                },
                userID: $scope.userID,
                launchpadCertified: false,
                pic: {
                    highres: $scope.imageURL
                }
            };
            $scope.showLoading = true;            
            craigsdigitalService.submitFreelancer($scope, formData);
            $scope.disableSubmit = true;
        } else {
            $scope.submitSuccess = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
            $scope.showLoading = false;
        }
    };
}]);

craigsdigitalApp.controller('searchFreelancersController',  ['$scope', 'craigsdigitalService', function($scope, craigsdigitalService) {
    var redirectURL;
    $scope.hideLoader = false;
    // Get all projects
     if(typeof(sessionStorage) !== "undefined") {
          redirectURL = sessionStorage.getItem('freelancerEndPoint') || '/api/getfreelancers';
          sessionStorage.removeItem('freelancerEndPoint');
     } else {
         redirectURL = '/api/getfreelancers';
     }
    
    craigsdigitalService.getAllFreelancers($scope, redirectURL);
    
    // Get filtered products and update product list
    $scope.filterFreelancers = function(){
        $scope.hideLoader = false;
        craigsdigitalService.getFilteredFreelancers($scope);
    };
    
    // Initialize -- Any changes below, make changes to /getfilteredfreelancers in api.js
    $scope.categoryList = [
        { id: 0, name: 'View All Freelancers'},
        { id: 1, name: 'Web & Mobile Development'},
        { id: 2, name: 'Tutoring '},
        { id: 3, name: 'Design & Creative'},
        { id: 4, name: 'Blogging & Writing'},
        { id: 5, name: 'Photography & Videography'},
        { id: 6, name: 'Other Creative Projects'}
    ];
    $scope.skillsList = [
        { id: 0, name: 'View All Skillset'},
        { id: 1, name: 'Javascript'},
        { id: 2, name: 'iOS'},
        { id: 3, name: 'Android'}
    ];
  
}]);