/* global craigsdigitalApp */
// Controller for create a gig page.

// Helper functions
// CSV to array
function convertCSVToArray(csv) {
    if(csv) {
        return csv.split(',').sort();            
    } else {
        return [];
    }        
};

craigsdigitalApp.controller('createGigController',  ['$scope', 'gigsService', function($scope, gigsService) {    
    
    $scope.showHelpText = function(){
        $scope.helpTextVisible = true;
    }
    
    $scope.payRateOptions = [
        { id: 0, name: 'Hourly'},
        { id: 1, name: 'Monthly'},
        { id: 2, name: 'Yearly'},
        { id: 3, name: 'Minimum cost'},
        { id: 4, name: 'Varies by project'}
    ];
    // Handle Delete User
    $scope.deleteUser = function(event) {
        event.preventDefault();
        var confirmMessage = confirm('Are you sure you want to delete this gig? This action cannot be undone.');
        if(confirmMessage === true) {
            gigsService.deleteGig($scope);                        
        }        
    };
    // handle submit form
    $scope.submitGigOne = function(isValid) {			
        $scope.submitted = true;
        $scope.disableSubmit = true;
        // check to make sure the form is completely valid        
        if (isValid) {
            var category = convertCSVToArray($scope.servicesRequired);
            $scope.showLoading = true;            
            gigsService.submitGig($scope, category);
            $scope.disableSubmit = true;
        } else {
            $scope.submitSuccess = false;
            $scope.submitError = true;
            $scope.disableSubmit = false;
            $scope.showLoading = false;
        }
    };
}]);


craigsdigitalApp.controller('searchGigs',  ['$scope', 'gigsService', function($scope, gigsService) {
    $scope.hideLoader = false;
    var redirectURL;
    
     if(typeof(sessionStorage) !== "undefined") {
          redirectURL = sessionStorage.getItem('categoryEndPoint') || '/api/allgigs';
          sessionStorage.removeItem('categoryEndPoint');
     } else {
         redirectURL = '/api/allgigs';
     }
    
    gigsService.getAllGigs($scope, redirectURL);
    
    // Get filtered products and update product list
    $scope.filterGigs = function(){
        $scope.hideLoader = false;
        gigsService.getFilteredGigs($scope);
    };
    
    // Initialize -- Any changes below, make changes to /getfilteredfreelancers in api.js
    $scope.categoryList = [
        { id: 0, name: 'View All Freelancers'},
        { id: 1, name: 'Web & Mobile Development'},
        { id: 2, name: 'Tutoring'},
        { id: 3, name: 'Design & Creative'},
        { id: 4, name: 'Blogging & Writing'},
        { id: 5, name: 'Photography & Videography'},
        { id: 6, name: 'Other Creative Projects'}
    ];
    $scope.skillsList = [
        { id: 0, name: 'View All Skillset'},
        { id: 1, name: 'Javascript'},
        { id: 2, name: 'iOS'},
        { id: 3, name: 'Android'}
    ];
  
}]);