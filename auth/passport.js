/* global process */
// Configuring passport

// Requiring modules
var request = require('request'),
    LocalStrategy = require('passport-local').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
    LinkedInStrategy = require('passport-linkedin-oauth2').Strategy,
    GoogleStrategy = require( 'passport-google-oauth2' ).Strategy,
	Project = require('../models/projects'),
    User = require('../models/users');
	
module.exports = function(passport, config) {
	
	// ================================================
    // passport session setup 
	// ================================================
	// Project for sessions
	passport.serializeUser(function(user, done){
		done(null, user.id);
	});
	
	// Project for mongoDB check
	passport.deserializeUser(function(id, done){
		User.findById(id, function(err, user){
			if(err) console.log(err);
			if(user) {
                done(err, user);
            } else {
                Project.findById(id, function(err, project){
                    done(err, project);
                });                
            }
		});
	});
	
	// ================================================
    // LOCAL SIGNUP PROJECT
	// ================================================
	passport.use('local-signup-project', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback: true // To pass back the request to the callback 
	},
	function(req, email, password, done) {
		process.nextTick(function(){
            var newProject = new Project();
            
            newProject.local.email = email;
            newProject.local.password = newProject.generateHash(password);
            newProject.local.category = req.body.category;
            newProject.local.title = req.body.title;
            newProject.local.payRate = req.body.payRate;
            newProject.local.expRequired = req.body.expRequired;
            newProject.local.duration = req.body.duration;
            newProject.local.phoneNumber = req.body.phoneNumber;
            newProject.local.skillset = req.body.skillset;
            newProject.local.description = req.body.description;
            newProject.local.launchpadCertified = req.body.launchpadCertified;
            
            newProject.save(function(err){
                if(err) console.log(err);
                return done(null, newProject, 'Form Saved Successfully.');
            });
		});
	}));
    
    // ================================================
    // LOCAL SIGNUP USER
	// ================================================
	passport.use('local-signup', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback: true // To pass back the request to the callback 
	},
	function(req, email, password, done) {
		// we want user.findOne to fire when data is sent back
		process.nextTick(function(){
			// Check if the user already exists
			User.findOne({'auth.local.email' : email}, function(err, user){
				if (err) return done(err);
				
				if(user) {
					return done(null, false, req.flash('signupMessage', 'Account with that email already exists!'));
				} else {
					// If an account does not exist, create one
					var newUser = new User();
					
					newUser.auth.local.email = email;
					newUser.auth.local.password = newUser.generateHash(password);
					
					// Save the details of the new user
					newUser.save(function(err){
						if(err) throw err;
						return done(null, newUser);
					});
				}
			});
		});
	}));
    
    passport.use('local-login', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback: true // To pass back the request to the callback 
	},
	function(req, email, password, done){
		User.findOne({'auth.local.email' : email}, function(err, user){
			if (err) return done(err);
			
			// If no user is found
			if(!user) {
				return done(null, false, req.flash('loginMessage', 'Account with this email does not exist!'));
			}
            
            if(user.auth.facebook.email || user.auth.google.email || user.auth.linkedin.email) {
				return done(null, false, req.flash('loginMessage', 'This email is linked with a social account. Use the buttons above to login.'));
			}
			
			if(!user.validPassword(password)) {
				return done(null, false, req.flash('loginMessage', 'Incorrect username/email combination. Please try again!'));
			}
			
			// If everything looks good, return user
			return done(null, user);
		});
	}));
    
    // =====================================
	// Facebook Strategy
	//======================================
	passport.use(new FacebookStrategy({
		// Get app details from config files.
		clientID : config.fb.appID,
		clientSecret : config.fb.appSecret,
		callbackURL : config.fb.callbackURL,
        profileFields: ['id', 'displayName', 'photos', 'email']	
	},
	
	function(token, refreshToken, profile, done) {
		
		process.nextTick(function(){
			
			// Find an existing user in DB
			User.findOne( { $or: [{'auth.facebook.id' : profile.id}, {'local.email' :  (profile.emails ? profile.emails[0].value : 'garbage@value.com')}]}, function(err, user){
				if(err) return done(err);
				
				// If a user is found
				if(user){
					return done(null, user);
				} else {
					// it is a new user
					var newUser = new User();
	
					newUser.auth.facebook.id = profile.id;
					newUser.auth.facebook.token = token;
					newUser.auth.facebook.name = (profile.name.firstName ? profile.name.firstName : '') + ' ' + (profile.name.lastName ? profile.name.lastName : '');
                    newUser.auth.local.name = newUser.auth.facebook.name;
                    newUser.auth.facebook.pic = {
                        lowres: profile.photos ? profile.photos[0].value : '',
                        highres: "https://graph.facebook.com/v2.4/" + profile.id + "/picture" + "?width=400&height=400" + "&access_token=" + token
                    };
                    newUser.auth.local.pic = newUser.auth.facebook.pic;
					newUser.auth.facebook.email = profile.emails ? profile.emails[0].value : '';
                    newUser.auth.local.email = newUser.auth.facebook.email;
                    newUser.auth.local.profileCompleted = false;
					
					newUser.save(function(err){
						if(err) console.log(err);
						
						return done(null, newUser);
					});
				}
			});
		});	
	}));
    
    // =====================================
	// Linkedin Strategy
	//======================================
    passport.use(new LinkedInStrategy({
		// Get app details from config files.
		clientID : config.linkedin.appID,
		clientSecret : config.linkedin.appSecret,
		callbackURL : config.linkedin.callbackURL,
        scope: ['r_emailaddress', 'r_basicprofile'],
        state: true
	},
	
	function(token, refreshToken, profile, done) {
		
		process.nextTick(function(){
			// Find an existing user in DB
			User.findOne({ $or: [{'auth.linkedin.id' : profile.id}, {'local.email' :  (profile.emails ? profile.emails[0].value : 'garbage@value.com')}]}, function(err, user){
				if(err) return done(err);
				
				// If a user is found
				if(user){
					return done(null, user);
				} else {
					// it is a new user
					var newUser = new User();
                    
                    // Get high res profile image.
                    request.get("https://api.linkedin.com/v1/people/~/picture-urls::(original)?format=json&oauth2_access_token=" + token,
                    function(error, response, body){
                        var data = JSON.parse(body);
                        // Save Data	
                        newUser.auth.linkedin.id = profile.id;
                        newUser.auth.linkedin.token = token;
                        newUser.auth.linkedin.name =  profile.displayName;
                        newUser.auth.name = newUser.auth.linkedin.name;
                        newUser.auth.linkedin.pic = {
                            lowres: profile.photos ? profile.photos[0].value : '',
                            highres: data.values[0]
                        };
                        newUser.auth.pic = newUser.auth.linkedin.pic;
                        newUser.auth.linkedin.email = profile.emails ? profile.emails[0].value : '';
                        newUser.auth.local.email = newUser.auth.linkedin.email;
                        newUser.auth.local.profileCompleted = false;
                        
                        newUser.save(function(err){
                            if(err) console.log(err);
                            
                            return done(null, newUser);
                        });
                     });
                }
			});
		});	
	}));
    
    
    // =====================================
	// Google Strategy
	//======================================
	passport.use(new GoogleStrategy({
		// Get app details from config files.
		clientID : config.google.appID,
		clientSecret : config.google.appSecret,
		callbackURL : config.google.callbackURL,
        passReqToCallback   : true
	},
	
	function(request, token, refreshToken, profile, done) {
		
		process.nextTick(function(){
			
			// Find an existing user in DB
			User.findOne({ $or: [{'auth.google.id' : profile.id}, {'local.email' :  profile.email }]}, function(err, user){
				if(err) return done(err);
				
				// If a user is found
				if(user){
					return done(null, user);
				} else {
					// it is a new user
					var newUser = new User();
                    
                    // Save Data	
					newUser.auth.google.id = profile.id;
					newUser.auth.google.token = token;
					newUser.auth.google.name = profile.displayName;
                    newUser.auth.local.name = newUser.auth.google.name;
                    newUser.auth.google.pic = {
                        lowres: profile.photos[0].value ? (profile.photos[0].value.split('?')[0] + '?sz=150') : '',
                        highres:profile.photos[0].value ? (profile.photos[0].value.split('?')[0] + '?sz=450') : '',
                    };
                    newUser.auth.local.pic = newUser.auth.google.pic;
					newUser.auth.google.email = profile.email;
					newUser.auth.local.email = newUser.auth.google.email;
                    newUser.auth.local.profileCompleted = false;
                    
					newUser.save(function(err){
						if(err) console.log(err);
						
						return done(null, newUser);
					});
				}
			});
		});	
	}));
};