var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var gigSchema = new Schema({
	userID: String,
    title: String,
	description: String,
	cost: Number,
    payRate: String,
	imageUrl: String,
    category: Schema.Types.Mixed,
	createdAt: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('Gig', gigSchema);