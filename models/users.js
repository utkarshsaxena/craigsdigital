// Files for users mongoose models

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	bcrypt = require('bcrypt-nodejs'),
	config = require('../config/config.js');
	
var userSchema  = new Schema({
	auth : {
		local : {
			email: String,
			password: String,
            name: String,
            professionalTitle: String,
            skillset: Schema.Types.Mixed,
            launchpadCertified: Boolean,
            phoneNumber: Number,
            createdAt : {
                type : Date, 
                default : Date.now()
            },
            updatedAt: String,
            overview: String,
            employmentHistory: Schema.Types.Mixed,
            education: Schema.Types.Mixed,
            mainServices: Schema.Types.Mixed,
            portfolioLinks: Schema.Types.Mixed,
            pic: Schema.Types.Mixed,
            availablityToWork: {
                id: Number,
                name: String
            },
            experienceLevel: {
                id: Number,
                name: String
            },
            profileCompleted: Boolean
        },
		facebook : {
			id : String,
			token : String,
			email : String,
			name : String,
            pic: {
                lowres: String,
                highres: String
            }
		},
        linkedin : {
			id : String,
			token : String,
			email : String,
			name : String,
            pic: {
                lowres: String,
                highres: String
            }
		},
        google : {
			id : String,
			token : String,
			email : String,
			name : String,
            pic: {
                lowres: String,
                highres: String
            }
		}
	}
});


// Add methods to userSchema

// Generate a hash for password
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

// Check if the user entered a valid password
userSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.auth.local.password);
}

module.exports = mongoose.model('User', userSchema);