// Files for projects mongoose models

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	bcrypt = require('bcrypt-nodejs'),
	config = require('../config/config.js');
	
var projectSchema  = new Schema({
    local : {
		email: String,
		password: String,
        category: Schema.Types.Mixed,
        title: String,
        description: String,
        payRate: Schema.Types.Mixed,
        expRequired: Schema.Types.Mixed,
        duration: Schema.Types.Mixed,
        phoneNumber: Number,
        skillset: Schema.Types.Mixed,
        launchpadCertified: Boolean,
        createdAt : {
            type : Date, 
            default : Date.now()
        }
    }	
});


// Add methods to projectSchema

// Generate a hash for password
projectSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

// Check if the user entered a valid password
projectSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.local.password);
}

module.exports = mongoose.model('Project', projectSchema);