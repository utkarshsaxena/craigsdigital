# README #

## Introduction:  

This is a platform to connect local freelancers in Vancouver to local clients with projects.

Click on the following link to view the application: **[The Work Desk](http://www.theworkdesk.ca)**


## Technologies: ##

* HTML/Swig Templating
* CSS/SASS
* Google Material Design
* AngularJs
* NodeJs/ExpressJs
* MongoDB
* Casper JS

## Purpose: ##

* Proficiency in MEAN stack development.
* Carry out usability tests with real users.
* Familiarize myself with Google Material Design.
* Create a strong team of developers and business students to promote the web application.

## Procedure: ##

1. Click on the following link to view the application: **[The Work Desk](http://www.theworkdesk.ca)**

## Timestamp: ##

**Dec, 2015 – June, 2016